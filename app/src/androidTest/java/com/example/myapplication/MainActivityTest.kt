package com.example.myapplication

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import androidx.test.core.app.ActivityScenario
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.Espresso.pressBack
import androidx.test.espresso.IdlingRegistry
import androidx.test.espresso.PerformException
import androidx.test.espresso.UiController
import androidx.test.espresso.ViewAction
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.contrib.RecyclerViewActions
import androidx.test.espresso.contrib.RecyclerViewActions.actionOnItemAtPosition
import androidx.test.espresso.matcher.ViewMatchers.*
import com.example.myapplication.models.DataDummy

import com.example.myapplication.utils.EspressoIdlingResource
import com.google.android.material.tabs.TabLayout
import org.hamcrest.Matchers.allOf
import org.junit.After
import org.junit.Before
import org.junit.Test

class MainActivityTest {

    @Before
    fun setup() {
        ActivityScenario.launch(HomeActivity::class.java)
        IdlingRegistry.getInstance().register(EspressoIdlingResource.getEspressoIdlingResource())
    }

    @After
    fun tearDown() {
        IdlingRegistry.getInstance().unregister(EspressoIdlingResource.getEspressoIdlingResource())
    }

    @Test
    fun loadMovie() {
        onView(withId(R.id.navigation_movie)).perform(click())
        onView(withId(R.id.recycle_view_content_movies))
            .check(matches(isDisplayed()))
        onView(withId(R.id.recycle_view_content_movies))
            .perform(RecyclerViewActions.scrollToPosition<RecyclerView.ViewHolder>(20))
    }

    @Test
    fun loadMovieDetail() {
        Thread.sleep(1_000)
        onView(withId(R.id.navigation_movie)).perform(click())
        Thread.sleep(1_000)
        onView(withId(R.id.recycle_view_content_movies))
            .perform(
                actionOnItemAtPosition<RecyclerView.ViewHolder>(
                    0,
                    click()
                )
            )
        Thread.sleep(1_000)
        onView(withId(R.id.movie_photo)).check(matches(isDisplayed()))
        onView(withId(R.id.content_title)).check(matches(isDisplayed()))
        onView(withId(R.id.content_title))
            .check(matches(withText(DataDummy.sampleDataMovieDummy()[0].title)))
        onView(withId(R.id.content_overview)).check(matches(isDisplayed()))
        onView(withId(R.id.content_overview))
            .check(matches(withText(DataDummy.sampleDataMovieDummy()[0].overview)))
        onView(withId(R.id.favourite_button)).perform(click())
        pressBack()
    }

    @Test
    fun loadShow() {
        onView(withId(R.id.navigation_show)).perform(click())
        onView(withId(R.id.recycle_view_content_shows))
            .check(matches(isDisplayed()))
        onView(withId(R.id.recycle_view_content_shows))
            .perform(RecyclerViewActions.scrollToPosition<RecyclerView.ViewHolder>(20))
    }

    @Test
    fun loadShowDetail() {
        Thread.sleep(1_000)
        onView(withId(R.id.navigation_show)).perform(click())
        Thread.sleep(1_000)
        onView(withId(R.id.recycle_view_content_shows))
            .perform(
                actionOnItemAtPosition<RecyclerView.ViewHolder>(
                    0,
                    click()
                )
            )
        Thread.sleep(1_000)
        onView(withId(R.id.movie_photo)).check(matches(isDisplayed()))
        onView(withId(R.id.content_title)).check(matches(isDisplayed()))
        onView(withId(R.id.content_title))
            .check(matches(withText(DataDummy.sampleDataShowDummy()[0].title)))
        onView(withId(R.id.content_overview)).check(matches(isDisplayed()))
        onView(withId(R.id.content_overview))
            .check(matches(withText(DataDummy.sampleDataShowDummy()[0].overview)))
        onView(withId(R.id.favourite_button)).perform(click())
        pressBack()
    }

    @Test
    fun loadFavMovie() {
        onView(withId(R.id.navigation_movie_fav)).perform(click())
        onView(withId(R.id.recycle_view_content_movies_fav))
            .check(matches(isDisplayed()))
        onView(withId(R.id.recycle_view_content_movies_fav))
            .perform(RecyclerViewActions.scrollToPosition<RecyclerView.ViewHolder>(1))
    }

    @Test
    fun loadFavMovieDetail() {
        Thread.sleep(1_000)
        onView(withId(R.id.navigation_movie_fav)).perform(click())
        Thread.sleep(1_000)
        onView(withId(R.id.recycle_view_content_movies_fav))
            .perform(
                actionOnItemAtPosition<RecyclerView.ViewHolder>(
                    0,
                    click()
                )
            )
        Thread.sleep(1_000)
        onView(withId(R.id.movie_photo)).check(matches(isDisplayed()))
        onView(withId(R.id.content_title)).check(matches(isDisplayed()))
        onView(withId(R.id.content_title))
            .check(matches(withText(DataDummy.sampleDataMovieDummy()[0].title)))
        onView(withId(R.id.content_overview)).check(matches(isDisplayed()))
        onView(withId(R.id.content_overview))
            .check(matches(withText(DataDummy.sampleDataMovieDummy()[0].overview)))
        onView(withId(R.id.favourite_button)).perform(click())
        pressBack()
    }

    @Test
    fun loadFavShow() {
        onView(withId(R.id.navigation_show_fav)).perform(click())
        onView(withId(R.id.recycle_view_content_shows_fav))
            .check(matches(isDisplayed()))
        onView(withId(R.id.recycle_view_content_shows_fav))
            .perform(RecyclerViewActions.scrollToPosition<RecyclerView.ViewHolder>(1))
    }

    @Test
    fun loadFavShowDetail() {
        Thread.sleep(1_000)
        onView(withId(R.id.navigation_show_fav)).perform(click())
        Thread.sleep(1_000)
        onView(withId(R.id.recycle_view_content_shows_fav))
            .perform(
                actionOnItemAtPosition<RecyclerView.ViewHolder>(
                    0,
                    click()
                )
            )
        Thread.sleep(1_000)
        onView(withId(R.id.movie_photo)).check(matches(isDisplayed()))
        onView(withId(R.id.content_title)).check(matches(isDisplayed()))
        onView(withId(R.id.content_title))
            .check(matches(withText(DataDummy.sampleDataMovieDummy()[0].title)))
        onView(withId(R.id.content_overview)).check(matches(isDisplayed()))
        onView(withId(R.id.content_overview))
            .check(matches(withText(DataDummy.sampleDataMovieDummy()[0].overview)))
        onView(withId(R.id.favourite_button)).perform(click())
        pressBack()
    }

    fun selectTabAtPosition(tabIndex: Int): ViewAction {
        return object : ViewAction {
            override fun getDescription() = "with tab at index $tabIndex"

            override fun getConstraints() =
                allOf(isDisplayed(), isAssignableFrom(TabLayout::class.java))

            override fun perform(uiController: UiController, view: View) {
                val tabLayout = view as TabLayout
                val tabAtIndex: TabLayout.Tab = tabLayout.getTabAt(tabIndex)
                    ?: throw PerformException.Builder()
                        .withCause(Throwable("No tab at index $tabIndex"))
                        .build()

                tabAtIndex.select()
            }
        }
    }
}