package com.example.myapplication.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.paging.PagedListAdapter
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.myapplication.R
import com.example.myapplication.data.source.lokal.entity.ShowEntity
import com.example.myapplication.databinding.ContentItemBinding
import com.example.myapplication.services.ServiceBuilder

class ShowsAdapter :
    PagedListAdapter<ShowEntity, ShowsAdapter.ListViewHolder>(DIFF_CALLBACK) {

    companion object {
        private val DIFF_CALLBACK = object : DiffUtil.ItemCallback<ShowEntity>() {
            override fun areItemsTheSame(oldItem: ShowEntity, newItem: ShowEntity): Boolean {
                return oldItem.id == newItem.id
            }

            override fun areContentsTheSame(oldItem: ShowEntity, newItem: ShowEntity): Boolean {
                return oldItem == newItem
            }
        }
    }

    private lateinit var onItemClickCallback: OnItemClickCallback

    fun setOnItemClickCallback(onItemClickCallback: OnItemClickCallback) {
        this.onItemClickCallback = onItemClickCallback
    }

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): ListViewHolder {

        val contentItemBinding =
            ContentItemBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return ListViewHolder(contentItemBinding)
    }

    override fun onBindViewHolder(holder: ShowsAdapter.ListViewHolder, position: Int) {
        val show = getItem(position)
        if (show != null) {
            holder.bind(show)
        }
    }

    inner class ListViewHolder(private val binding: ContentItemBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(show: ShowEntity) {
            with(binding) {
                itemName.text = show.name
                itemPopularity.text = show.popularity

                Glide.with(itemPhoto.context)
                    .load(ServiceBuilder.imageUrl + show.poster_path)
                    .placeholder(R.drawable.ic_launcher_foreground)
                    .error(R.drawable.ic_launcher_foreground)
                    .fallback(R.drawable.ic_launcher_foreground)
                    .centerCrop()
                    .into(itemPhoto)

                itemView.setOnClickListener { onItemClickCallback.onItemClicked(show.id) }
            }
        }
    }

    interface OnItemClickCallback {
        fun onItemClicked(id: String)
    }
}
