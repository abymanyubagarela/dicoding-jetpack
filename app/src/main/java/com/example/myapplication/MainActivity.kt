package com.example.myapplication

import android.os.Bundle
import androidx.annotation.StringRes
import androidx.appcompat.app.AppCompatActivity
import androidx.viewpager2.widget.ViewPager2
import com.example.myapplication.adapters.HomePagerAdapter
import com.google.android.material.tabs.TabLayout
import com.google.android.material.tabs.TabLayoutMediator

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        beforeView()
    }

    private fun beforeView() {
        val homePagerAdapter = HomePagerAdapter(this)
        val viewPager: ViewPager2 = findViewById(R.id.id_view_pager)
        viewPager.adapter = homePagerAdapter
        val tabs: TabLayout = findViewById(R.id.id_tabs)
        TabLayoutMediator(tabs, viewPager) { tab, position ->
            tab.text = resources.getString(TAB_TITLES[position])
        }.attach()
    }

    companion object {
        @StringRes
        private val TAB_TITLES = intArrayOf(
            R.string.movies_tab,
            R.string.shows_tab
        )
    }
}
