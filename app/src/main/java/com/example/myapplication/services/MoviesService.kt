package com.example.myapplication.services

import com.example.myapplication.data.source.remote.response.ImdbResponse
import com.example.myapplication.models.Movies
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface MoviesService {
    @GET("movie/popular")
    fun getMovieList(@Query("api_key") apiKey: String = ServiceBuilder.apiKey): Call<ImdbResponse>

    @GET("movie/{id}")
    fun getMovie (@Path("id") id: String?, @Query("api_key") apiKey: String = ServiceBuilder.apiKey) : Call<Movies>
}