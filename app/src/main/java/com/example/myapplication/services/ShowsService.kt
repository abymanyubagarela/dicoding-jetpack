package com.example.myapplication.services

import com.example.myapplication.data.source.remote.response.ImdbResponseShow
import com.example.myapplication.models.Shows
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface ShowsService {
    @GET("tv/popular")
    fun getShowList(@Query("api_key") apiKey: String = ServiceBuilder.apiKey): Call<ImdbResponseShow>

    @GET("tv/{id}")
    fun getShow (@Path("id") id: String?, @Query("api_key") apiKey: String = ServiceBuilder.apiKey) : Call<Shows>
}