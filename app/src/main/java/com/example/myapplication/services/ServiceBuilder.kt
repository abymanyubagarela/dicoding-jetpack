package com.example.myapplication.services
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

object ServiceBuilder {
    const val URL = "https://api.themoviedb.org/3/"

    const val apiKey = "3c1ffe2211849e8a0ccc4f3029ca05ee"

    const val imageUrl = "https://image.tmdb.org/t/p/w500"

    //CREATE HTTP CLIENT
    private val okHttp = OkHttpClient.Builder()

    //retrofit builder
    private val builder = Retrofit.Builder().baseUrl(URL)
        .addConverterFactory(GsonConverterFactory.create())
        .client(okHttp.build())

    //create retrofit Instance
    private val retrofit = builder.build()

    //we will use this class to create an anonymous inner class function that
    //implements Country service Interface
    fun <T> buildService (serviceType :Class<T>):T{
        return retrofit.create(serviceType)
    }
}
