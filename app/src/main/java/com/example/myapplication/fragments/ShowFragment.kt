package com.example.myapplication.fragments

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.paging.PagedList
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.myapplication.DetailsActivity
import com.example.myapplication.R
import com.example.myapplication.adapters.ShowsAdapter
import com.example.myapplication.data.source.lokal.entity.ShowEntity
import com.example.myapplication.databinding.ContainerFragmentShowsBinding
import com.example.myapplication.views.ShowsViewModel
import com.example.myapplication.views.ViewModelFactory
import com.example.myapplication.vo.Resource
import com.example.myapplication.vo.Status

class ShowFragment : Fragment(), ShowsAdapter.OnItemClickCallback {
    private lateinit var recyclerView: RecyclerView
    private lateinit var showBinding: ContainerFragmentShowsBinding
    private lateinit var showAdapter: ShowsAdapter

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        showBinding = ContainerFragmentShowsBinding.inflate(layoutInflater, container, false)
        setHasOptionsMenu(true)
        return showBinding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        recyclerView = view.findViewById(R.id.recycle_view_content_shows)
        recyclerView.setHasFixedSize(true)
        recyclerView.layoutManager =
            LinearLayoutManager(view.context, LinearLayoutManager.VERTICAL, false)

        val factory = ViewModelFactory.getInstance(requireActivity())
        val viewModel = ViewModelProvider(this, factory)[ShowsViewModel::class.java]
        viewModel.fetchShows().observe(viewLifecycleOwner, showObserver)

        showAdapter = ShowsAdapter()
        with(showBinding.recycleViewContentShows) {
            layoutManager = LinearLayoutManager(context)
            setHasFixedSize(true)
            this.adapter = showAdapter
        }
    }

    private val showObserver = Observer<Resource<PagedList<ShowEntity>>> { shows ->
        if (shows != null) {
            when (shows.status) {
                Status.SUCCESS -> {
                    showAdapter.submitList(shows.data)
                    showAdapter.setOnItemClickCallback(this)
                    showAdapter.notifyDataSetChanged()
                }
                Status.ERROR -> {
                    Toast.makeText(context, "Terjadi kesalahan", Toast.LENGTH_SHORT).show()
                }
            }
        }
    }

    override fun onItemClicked(id: String) {
        val intent = Intent(activity, DetailsActivity::class.java)
        intent.putExtra(DetailsActivity.TYPE, 2)
        intent.putExtra(DetailsActivity.ID, id)
        startActivity(intent)

        context?.startActivity(intent)
    }

    companion object {
        private const val ARG_SECTION_INDEX = "ARG_SECTION_INDEX"
        fun newInstance(index: Int): ShowFragment {
            val bundle = Bundle().apply {
                putInt(ARG_SECTION_INDEX, index)
            }
            return ShowFragment().apply {
                arguments = bundle
            }
        }
    }
}