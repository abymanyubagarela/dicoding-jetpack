package com.example.myapplication.fragments

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.observe
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.myapplication.DetailsActivity
import com.example.myapplication.R
import com.example.myapplication.adapters.MovieAdapter
import com.example.myapplication.databinding.FragmentFavMovieBinding
import com.example.myapplication.views.MoviesViewModel
import com.example.myapplication.views.ViewModelFactory

class FavMovieFragment : Fragment(), MovieAdapter.OnItemClickCallback {
    private lateinit var recyclerView: RecyclerView
    private lateinit var movieBinding: FragmentFavMovieBinding
    private lateinit var movieAdapter: MovieAdapter

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        movieBinding = FragmentFavMovieBinding.inflate(layoutInflater, container, false)
        setHasOptionsMenu(true)
        return movieBinding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        recyclerView = view.findViewById(R.id.recycle_view_content_movies_fav)
        recyclerView.setHasFixedSize(true)
        recyclerView.layoutManager =
            LinearLayoutManager(view.context, LinearLayoutManager.VERTICAL, false)

        val factory = ViewModelFactory.getInstance(requireActivity())
        val viewModel = ViewModelProvider(this, factory)[MoviesViewModel::class.java]

        viewModel.fetchFavMovies().observe(viewLifecycleOwner) { movies ->
            if (movies != null) {
                movieAdapter.submitList(movies)
                movieAdapter.setOnItemClickCallback(this)
                movieAdapter.notifyDataSetChanged()
            }
        }

        movieAdapter = MovieAdapter()
        with(movieBinding.recycleViewContentMoviesFav) {
            layoutManager = LinearLayoutManager(context)
            setHasFixedSize(true)
            this.adapter = movieAdapter
        }
    }

    override fun onItemClicked(id: String) {
        val intent = Intent(activity, DetailsActivity::class.java)
        intent.putExtra(DetailsActivity.TYPE, 1)
        intent.putExtra(DetailsActivity.ID, id)
        startActivity(intent)

        context?.startActivity(intent)
    }
}