package com.example.myapplication.fragments

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.observe
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.myapplication.DetailsActivity
import com.example.myapplication.R
import com.example.myapplication.adapters.ShowsAdapter
import com.example.myapplication.databinding.FragmentFavShowBinding
import com.example.myapplication.views.ShowsViewModel
import com.example.myapplication.views.ViewModelFactory

class FavShowFragment : Fragment(), ShowsAdapter.OnItemClickCallback {
    private lateinit var recyclerView: RecyclerView
    private lateinit var showBinding: FragmentFavShowBinding
    private lateinit var showAdapter: ShowsAdapter

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        showBinding = FragmentFavShowBinding.inflate(layoutInflater, container, false)
        setHasOptionsMenu(true)
        return showBinding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        recyclerView = view.findViewById(R.id.recycle_view_content_shows_fav)
        recyclerView.setHasFixedSize(true)
        recyclerView.layoutManager =
            LinearLayoutManager(view.context, LinearLayoutManager.VERTICAL, false)

        val factory = ViewModelFactory.getInstance(requireActivity())
        val viewModel = ViewModelProvider(this, factory)[ShowsViewModel::class.java]

        viewModel.fetchFavShows().observe(viewLifecycleOwner) { shows ->
            if (shows != null) {
                showAdapter.submitList(shows)
                showAdapter.setOnItemClickCallback(this)
                showAdapter.notifyDataSetChanged()
            }
        }
        showAdapter = ShowsAdapter()
        with(showBinding.recycleViewContentShowsFav) {
            layoutManager = LinearLayoutManager(context)
            setHasFixedSize(true)
            this.adapter = showAdapter
        }
    }

    override fun onItemClicked(id: String) {
        val intent = Intent(activity, DetailsActivity::class.java)
        intent.putExtra(DetailsActivity.TYPE, 2)
        intent.putExtra(DetailsActivity.ID, id)
        startActivity(intent)

        context?.startActivity(intent)
    }
}