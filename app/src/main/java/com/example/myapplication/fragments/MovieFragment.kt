package com.example.myapplication.fragments

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.paging.PagedList
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.myapplication.DetailsActivity
import com.example.myapplication.R
import com.example.myapplication.adapters.MovieAdapter
import com.example.myapplication.data.source.lokal.entity.MovieEntity
import com.example.myapplication.databinding.ContainerFragmentMovieBinding
import com.example.myapplication.views.MoviesViewModel
import com.example.myapplication.views.ViewModelFactory
import com.example.myapplication.vo.Resource
import com.example.myapplication.vo.Status


class MovieFragment : Fragment(), MovieAdapter.OnItemClickCallback {
    private lateinit var recyclerView: RecyclerView
    private lateinit var movieBinding: ContainerFragmentMovieBinding
    private lateinit var movieAdapter: MovieAdapter

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        movieBinding = ContainerFragmentMovieBinding.inflate(layoutInflater, container, false)
        setHasOptionsMenu(true)
        return movieBinding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        recyclerView = view.findViewById(R.id.recycle_view_content_movies)
        recyclerView.setHasFixedSize(true)
        recyclerView.layoutManager =
            LinearLayoutManager(view.context, LinearLayoutManager.VERTICAL, false)

        val factory = ViewModelFactory.getInstance(requireActivity())
        val viewModel = ViewModelProvider(this, factory)[MoviesViewModel::class.java]
        viewModel.fetchMovies().observe(viewLifecycleOwner, movieObserver)

        movieAdapter = MovieAdapter()
        with(movieBinding.recycleViewContentMovies) {
            layoutManager = LinearLayoutManager(context)
            setHasFixedSize(true)
            this.adapter = movieAdapter
        }
    }

    private val movieObserver = Observer<Resource<PagedList<MovieEntity>>> { movies ->
        if (movies != null) {
            when (movies.status) {
                Status.SUCCESS -> {
                    movieAdapter.submitList(movies.data)
                    movieAdapter.setOnItemClickCallback(this)
                    movieAdapter.notifyDataSetChanged()
                }
                Status.ERROR -> {
                    Toast.makeText(context, "Terjadi kesalahan", Toast.LENGTH_SHORT).show()
                }
            }
        }
    }

    override fun onItemClicked(id: String) {
        val intent = Intent(activity, DetailsActivity::class.java)
        intent.putExtra(DetailsActivity.TYPE, 1)
        intent.putExtra(DetailsActivity.ID, id)
        startActivity(intent)

        context?.startActivity(intent)
    }

    companion object {
        private const val ARG_SECTION_INDEX = "ARG_SECTION_INDEX"
        fun newInstance(index: Int): MovieFragment {
            val bundle = Bundle().apply {
                putInt(ARG_SECTION_INDEX, index)
            }
            return MovieFragment().apply {
                arguments = bundle
            }
        }
    }
}