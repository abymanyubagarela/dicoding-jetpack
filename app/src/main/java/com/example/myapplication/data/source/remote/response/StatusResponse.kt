package com.example.myapplication.data.source.remote.response

enum class StatusResponse {
    SUCCESS,
    EMPTY,
    ERROR
}