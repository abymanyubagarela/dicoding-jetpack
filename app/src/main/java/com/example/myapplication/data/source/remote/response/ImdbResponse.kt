package com.example.myapplication.data.source.remote.response

import com.example.myapplication.models.Movies
import com.example.myapplication.models.Shows

data class ImdbResponse(
    var page: Int,
    var results: List<Movies>,
    var total_pages: Int,
    var total_results: Int
)

data class ImdbResponseShow(
    var page: Int,
    var results: List<Shows>,
    var total_pages: Int,
    var total_results: Int
)