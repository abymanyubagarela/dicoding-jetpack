package com.example.myapplication.data.source.lokal.room

import androidx.lifecycle.LiveData
import androidx.paging.DataSource
import androidx.room.*
import com.example.myapplication.data.source.lokal.entity.MovieEntity
import com.example.myapplication.data.source.lokal.entity.ShowEntity

@Dao
interface ImdbDao {
    // movie dao
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertMovies(movies: List<MovieEntity>)

    @Update
    fun updateMovie(movie: MovieEntity)

    @Delete
    fun deleteMovie(movie: MovieEntity)

    @Query("SELECT * FROM movie WHERE id = :id")
    fun getMovieById(id: String): LiveData<MovieEntity>

    @Query("SELECT * FROM movie WHERE isFav = 1")
    fun getFavMovies(): DataSource.Factory<Int, MovieEntity>

    @Query("SELECT * FROM movie")
    fun getAllMovie(): DataSource.Factory<Int, MovieEntity>

    // show dao
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertShows(shows: List<ShowEntity>)

    @Update
    fun updateShow(show: ShowEntity)

    @Query("UPDATE show SET isFav=:isFav WHERE id = :id")
    fun updateShowById(isFav: Boolean, id: String)

    @Delete
    fun deleteShow(show: ShowEntity)

    @Query("SELECT * FROM show WHERE id = :id")
    fun getShowById(id: String): LiveData<ShowEntity>

    @Query("SELECT * from show ORDER BY id ASC")
    fun getAllShow(): DataSource.Factory<Int, ShowEntity>

    @Query("SELECT * FROM show WHERE isFav = 1")
    fun getFavShows(): DataSource.Factory<Int, ShowEntity>
}
