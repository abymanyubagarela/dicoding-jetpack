package com.example.myapplication.data.source

import androidx.lifecycle.LiveData
import androidx.paging.PagedList
import com.example.myapplication.data.source.lokal.entity.MovieEntity
import com.example.myapplication.data.source.lokal.entity.ShowEntity
import com.example.myapplication.vo.Resource

interface ImdbDataSource {
    // movie
    fun fetchMovies(): LiveData<Resource<PagedList<MovieEntity>>>
    fun fetchMovie(id: String?): LiveData<Resource<MovieEntity>>
    fun getFavoriteMovies(): LiveData<PagedList<MovieEntity>>
    fun setFavoriteMovie(movie: MovieEntity, state: Boolean)

    // show
    fun fetchShows(): LiveData<Resource<PagedList<ShowEntity>>>
    fun fetchShow(id: String?): LiveData<Resource<ShowEntity>>
    fun getFavoriteShows(): LiveData<PagedList<ShowEntity>>
    fun setFavoriteShow(show: ShowEntity, state: Boolean)
}