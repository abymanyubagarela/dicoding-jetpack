package com.example.myapplication.data.source.lokal.room

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.example.myapplication.data.source.lokal.entity.MovieEntity
import com.example.myapplication.data.source.lokal.entity.ShowEntity

@Database(
    entities = [MovieEntity::class, ShowEntity::class],
    version = 1
)
abstract class ImdbRoomDatabase : RoomDatabase() {
    abstract fun imdbDao(): ImdbDao

    companion object {
        @Volatile
        private var INSTANCE: ImdbRoomDatabase? = null

        fun getInstance(context: Context): ImdbRoomDatabase =
            INSTANCE ?: synchronized(this) {
                INSTANCE
                    ?: Room.databaseBuilder(
                        context.applicationContext,
                        ImdbRoomDatabase::class.java,
                        "Imdb.db"
                    ).build()
            }
    }
}