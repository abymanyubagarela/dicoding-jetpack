package com.example.myapplication.data.source.remote

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.example.myapplication.data.source.remote.response.ApiResponse
import com.example.myapplication.data.source.remote.response.ImdbResponse
import com.example.myapplication.data.source.remote.response.ImdbResponseShow
import com.example.myapplication.models.Movies
import com.example.myapplication.models.Shows
import com.example.myapplication.services.MoviesService
import com.example.myapplication.services.ServiceBuilder
import com.example.myapplication.services.ShowsService
import com.example.myapplication.utils.EspressoIdlingResource
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class RemoteDataSource {
    private val movieService = ServiceBuilder.buildService(MoviesService::class.java)
    private val showService = ServiceBuilder.buildService(ShowsService::class.java)

    companion object {
        @Volatile
        private var instance: RemoteDataSource? = null

        fun getInstance(): RemoteDataSource =
            instance ?: synchronized(this) {
                instance ?: RemoteDataSource()
            }
    }

    fun fetchMovies(): LiveData<ApiResponse<List<Movies>>> {
        EspressoIdlingResource.increment()

        val result = MutableLiveData<ApiResponse<List<Movies>>>()
        val client = movieService.getMovieList()

        client.enqueue(object : Callback<ImdbResponse> {
            override fun onResponse(call: Call<ImdbResponse>, response: Response<ImdbResponse>) {
                result.value = ApiResponse.success(response.body()?.results as List<Movies>)
                EspressoIdlingResource.decrement()
            }

            override fun onFailure(call: Call<ImdbResponse>, t: Throwable) {
                Log.d("RemoteDataSource", "Error: ${t}")
                EspressoIdlingResource.decrement()
            }
        })

        return result
    }

    fun fetchShows(): LiveData<ApiResponse<List<Shows>>> {
        EspressoIdlingResource.increment()

        val result = MutableLiveData<ApiResponse<List<Shows>>>()
        val client = showService.getShowList()

        client.enqueue(object : Callback<ImdbResponseShow> {
            override fun onResponse(
                call: Call<ImdbResponseShow>,
                response: Response<ImdbResponseShow>
            ) {
                result.value = ApiResponse.success(response.body()?.results as List<Shows>)
                EspressoIdlingResource.decrement()
            }

            override fun onFailure(call: Call<ImdbResponseShow>, t: Throwable) {
                Log.d("RemoteDataSource", "Error: ${t}")
                EspressoIdlingResource.decrement()
            }

        })
        return result
    }

    fun fetchMovie(id: String): LiveData<ApiResponse<Movies>> {
        EspressoIdlingResource.increment()

        val result = MutableLiveData<ApiResponse<Movies>>()
        val client = movieService.getMovie(id)

        client.enqueue(object : Callback<Movies> {
            override fun onResponse(call: Call<Movies>, response: Response<Movies>) {
                result.value = ApiResponse.success(response.body() as Movies)
                EspressoIdlingResource.decrement()
            }

            override fun onFailure(call: Call<Movies>, t: Throwable) {
                Log.d("RemoteDataSource", "Error: ${t}")
                EspressoIdlingResource.decrement()
            }

        })
        return result
    }

    fun fetchShow(id: String): LiveData<ApiResponse<Shows>> {
        EspressoIdlingResource.increment()

        val result = MutableLiveData<ApiResponse<Shows>>()
        val client = showService.getShow(id)

        client.enqueue(object : Callback<Shows> {
            override fun onResponse(call: Call<Shows>, response: Response<Shows>) {
                result.value = ApiResponse.success(response.body() as Shows)
                EspressoIdlingResource.decrement()
            }

            override fun onFailure(call: Call<Shows>, t: Throwable) {
                Log.d("RemoteDataSource", "Error: ${t}")
                EspressoIdlingResource.decrement()
            }

        })
        return result
    }
}