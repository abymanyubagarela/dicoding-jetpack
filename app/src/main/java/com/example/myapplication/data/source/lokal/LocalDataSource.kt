package com.example.myapplication.data.source.lokal

import androidx.lifecycle.LiveData
import androidx.paging.DataSource
import com.example.myapplication.data.source.lokal.entity.MovieEntity
import com.example.myapplication.data.source.lokal.entity.ShowEntity
import com.example.myapplication.data.source.lokal.room.ImdbDao

class LocalDataSource(private val mImdbDao: ImdbDao) {
    companion object {
        private var INSTANCE: LocalDataSource? = null

        fun getInstance(imdbDao: ImdbDao): LocalDataSource =
            INSTANCE ?: LocalDataSource(imdbDao)
    }

    // data source movie
    fun getAllMovies(): DataSource.Factory<Int, MovieEntity> = mImdbDao.getAllMovie()

    fun getAllFavMovies(): DataSource.Factory<Int, MovieEntity> = mImdbDao.getFavMovies()

    fun getMovieById(id: String?): LiveData<MovieEntity> = mImdbDao.getMovieById(id.toString())

    fun insertMovies(movies: List<MovieEntity>) = mImdbDao.insertMovies(movies)

    fun updateMovie(movie: MovieEntity, newState: Boolean) {
        movie.isFav = newState
        mImdbDao.updateMovie(movie)
    }

    fun deleteMovies(movies: MovieEntity) = mImdbDao.deleteMovie(movies)

    fun setFavMovie(movie: MovieEntity, newState: Boolean) {
        movie.isFav = newState
        mImdbDao.updateMovie(movie)
    }

    // data source show
    fun getAllShows(): DataSource.Factory<Int, ShowEntity> = mImdbDao.getAllShow()

    fun getAllFavShows(): DataSource.Factory<Int, ShowEntity> = mImdbDao.getFavShows()

    fun getShowById(id: String): LiveData<ShowEntity> = mImdbDao.getShowById(id.toString())

    fun insertShows(shows: List<ShowEntity>) = mImdbDao.insertShows(shows)

    fun updateShow(show: ShowEntity, newState: Boolean) {
        show.isFav = newState
        mImdbDao.updateShow(show)
    }

    fun deleteShows(show: ShowEntity) = mImdbDao.deleteShow(show)

    fun setFavShow(show: ShowEntity, newState: Boolean) {
        show.isFav = newState
        mImdbDao.updateShow(show)
    }

    fun setFavShowById(isFav: Boolean, id: String) {
        mImdbDao.updateShowById(isFav, id)
    }
}