package com.example.myapplication.models

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Movies (
    var id:String? = null,
    var title:String? = null,
    var overview:String? = null,
    var popularity:String? = null,
    var poster_path:String? = null,
    var vote_average:String? = null,
    var vote_count:String? = null,
    var backdrop_path:String? = null
) : Parcelable
