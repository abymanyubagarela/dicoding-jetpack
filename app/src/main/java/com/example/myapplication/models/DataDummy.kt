package com.example.myapplication.models

import com.example.myapplication.data.source.lokal.entity.MovieEntity
import com.example.myapplication.data.source.lokal.entity.ShowEntity

object DataDummy {
    fun sampleDataMovieDummy(): List<MovieEntity> {
        val listMovie = ArrayList<MovieEntity>()
        listMovie.add(
            MovieEntity(
                "337404",
                "Cruella",
                "In 1970s London amidst the punk rock revolution, a young grifter named Estella is determined to make a name for herself with her designs. She befriends a pair of young thieves who appreciate her appetite for mischief, and together they are able to build a life for themselves on the London streets. One day, Estella’s flair for fashion catches the eye of the Baroness von Hellman, a fashion legend who is devastatingly chic and terrifyingly haute. But their relationship sets in motion a course of events and revelations that will cause Estella to embrace her wicked side and become the raucous, fashionable and revenge-bent Cruella.",
                "4387.63",
                "/rTh4K5uw9HypmpGslcKd4QfHl93.jpg",
                "8.6",
                "3086",
                "/8ChCpCYxh9YXusmHwcE9YzP0TSG.jpg",
                false
            )
        )
        listMovie.add(
            MovieEntity(
                "337404",
                "Cruella",
                "In 1970s London amidst the punk rock revolution, a young grifter named Estella is determined to make a name for herself with her designs. She befriends a pair of young thieves who appreciate her appetite for mischief, and together they are able to build a life for themselves on the London streets. One day, Estella’s flair for fashion catches the eye of the Baroness von Hellman, a fashion legend who is devastatingly chic and terrifyingly haute. But their relationship sets in motion a course of events and revelations that will cause Estella to embrace her wicked side and become the raucous, fashionable and revenge-bent Cruella.",
                "4387.63",
                "/rTh4K5uw9HypmpGslcKd4QfHl93.jpg",
                "8.6",
                "3086",
                "/8ChCpCYxh9YXusmHwcE9YzP0TSG.jpg",
                false
            )
        )
        return listMovie
    }

    fun sampleDataShowDummy(): List<ShowEntity> {
        val listShows = ArrayList<ShowEntity>()
        listShows.add(
            ShowEntity(
                "103768",
                "Sweet Tooth",
                "Sweet Tooth",
                "On a perilous adventure across a post-apocalyptic world, a lovable boy who's half-human and half-deer searches for a new beginning with a gruff protector.",
                "861.919",
                "/rgMfhcrVZjuy5b7Pn0KzCRCEnMX.jpg",
                "8",
                "433",
                "/xpba0Dxz3sxV3QgYLR8UIe1LAAX.jpg",
                false
            )
        )
        return listShows
    }
}