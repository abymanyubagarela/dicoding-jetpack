package com.example.myapplication.di

import android.content.Context
import com.example.myapplication.data.source.ImdbRepository
import com.example.myapplication.data.source.lokal.LocalDataSource
import com.example.myapplication.data.source.lokal.room.ImdbRoomDatabase
import com.example.myapplication.data.source.remote.RemoteDataSource
import com.example.myapplication.utils.AppExecutors

object Injection {
    fun provideRepository(context: Context): ImdbRepository {

        val database = ImdbRoomDatabase.getInstance(context)
        val remoteDataSource = RemoteDataSource.getInstance()
        val localDataSource = LocalDataSource.getInstance(database.imdbDao())
        val appExecutors = AppExecutors()

        return ImdbRepository.getInstance(remoteDataSource, localDataSource, appExecutors)
    }
}