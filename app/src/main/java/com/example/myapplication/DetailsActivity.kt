package com.example.myapplication

import android.os.Bundle
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.observe
import com.bumptech.glide.Glide
import com.example.myapplication.services.ServiceBuilder
import com.example.myapplication.views.MoviesViewModel
import com.example.myapplication.views.ShowsViewModel
import com.example.myapplication.views.ViewModelFactory
import com.example.myapplication.vo.Status
import com.google.android.material.floatingactionbutton.FloatingActionButton

class DetailsActivity : AppCompatActivity() {
    private lateinit var fav_button: FloatingActionButton
    private var title: String = ""
    private var overview: String = ""
    private var poster: String = ""
    private var isfav: Boolean = false
    private var id: String = ""
    private var type: Int = 1

    private val factory = ViewModelFactory.getInstance(this)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_details)

        val actionbar = supportActionBar
        actionbar?.apply {
            actionbar.title = "Overview"
            actionbar.setDisplayHomeAsUpEnabled(true)
        }

        fav_button = findViewById<FloatingActionButton>(R.id.favourite_button)
        fav_button.setOnClickListener {
            onFabClicked()
        }

        id = intent.getStringExtra(ID).toString()
        type = intent.getIntExtra(TYPE, 10)

        when (type) {
            1 -> getMovie(id)
            2 -> getShows(id)
        }
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }

    fun getShows(id: String) {
        val viewModelShow = ViewModelProvider(this, factory)[ShowsViewModel::class.java]

        viewModelShow.fetchShow(id).observe(this) { show ->
            when (show.status) {
                Status.SUCCESS -> {
                    if (show.data != null) {
                        title = show.data.name
                        overview = show.data.overview
                        isfav = show.data.isFav
                        poster = ServiceBuilder.imageUrl + show.data.poster_path
                        patchView()
                    }
                }
                Status.ERROR -> {
                    Toast.makeText(applicationContext, "Terjadi kesalahan", Toast.LENGTH_SHORT)
                        .show()
                }
            }
        }
    }

    fun getMovie(id: String) {
        val viewModelMovie = ViewModelProvider(this, factory)[MoviesViewModel::class.java]

        viewModelMovie.fetchMovie(id).observe(this) { movie ->
            when (movie.status) {
                Status.SUCCESS -> {
                    if (movie.data != null) {
                        title = movie.data.title
                        overview = movie.data.overview
                        isfav = movie.data.isFav
                        poster = ServiceBuilder.imageUrl + movie.data.poster_path
                        patchView()
                    }
                }
                Status.ERROR -> {
                    Toast.makeText(applicationContext, "Terjadi kesalahan", Toast.LENGTH_SHORT)
                        .show()
                }
            }
        }
    }

    fun patchView() {
        val movie_photo: ImageView = findViewById(R.id.movie_photo)
        val movie_title: TextView = findViewById(R.id.content_title)
        val movie_overview: TextView = findViewById(R.id.content_overview)

        movie_title.text = title
        movie_overview.text = overview
        setFavorite(isfav)
        Glide.with(this)
            .load(poster)
            .placeholder(R.drawable.ic_launcher_foreground)
            .error(R.drawable.ic_launcher_foreground)
            .fallback(R.drawable.ic_launcher_foreground)
            .centerCrop()
            .into(movie_photo)
    }

    private fun setFavorite(state: Boolean) {
        if (state) {
            fav_button.setImageResource(R.drawable.ic_action_favourite_on)
        } else {
            fav_button.setImageResource(R.drawable.ic_action_favourite)
        }
    }

    private fun onFabClicked() {
        if (type == 1) {
            val viewModelMovie = ViewModelProvider(this, factory)[MoviesViewModel::class.java]
            viewModelMovie.setFavoriteMovie()
        } else if (type == 2) {
            val viewModelShow = ViewModelProvider(this, factory)[ShowsViewModel::class.java]
            viewModelShow.setFavoriteShow()
        }
        setFavorite(!isfav)
    }

    companion object {
        const val TYPE = "TYPE"
        const val ID = "ID"
    }
}