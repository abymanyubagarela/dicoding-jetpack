package com.example.myapplication.views

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import androidx.paging.PagedList
import com.example.myapplication.data.source.ImdbRepository
import com.example.myapplication.data.source.lokal.entity.ShowEntity
import com.example.myapplication.vo.Resource

class ShowsViewModel(private val imdbRepository: ImdbRepository) : ViewModel() {
    private lateinit var detailShow: LiveData<Resource<ShowEntity>>

    fun fetchShows(): LiveData<Resource<PagedList<ShowEntity>>> = imdbRepository.fetchShows()

    fun fetchFavShows(): LiveData<PagedList<ShowEntity>> = imdbRepository.getFavoriteShows()

    fun fetchShow(id: String?): LiveData<Resource<ShowEntity>> {
        detailShow = imdbRepository.fetchShow(id)
        return detailShow
    }

    fun setFavoriteShow() {
        val resource = detailShow.value
        if (resource?.data != null) {
            val newState = !resource.data.isFav
            imdbRepository.setFavoriteShow(resource.data, newState)
        }
    }

    fun fakeSetFavoriteShow(showEntity: ShowEntity) {
        val resource = showEntity
        if (resource != null) {
            val newState = !resource.isFav
            imdbRepository.setFavoriteShow(resource, newState)
        }
    }
}
