package com.example.myapplication.views

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import androidx.paging.PagedList
import com.example.myapplication.data.source.ImdbRepository
import com.example.myapplication.data.source.lokal.entity.MovieEntity
import com.example.myapplication.vo.Resource

class MoviesViewModel(private val imdbRepository: ImdbRepository) : ViewModel() {
    private lateinit var detailMovie: LiveData<Resource<MovieEntity>>

    fun fetchMovies(): LiveData<Resource<PagedList<MovieEntity>>> = imdbRepository.fetchMovies()

    fun fetchFavMovies(): LiveData<PagedList<MovieEntity>> = imdbRepository.getFavoriteMovies()

    fun fetchMovie(id: String?): LiveData<Resource<MovieEntity>> {
        detailMovie = imdbRepository.fetchMovie(id)
        return detailMovie
    }

    fun setFavoriteMovie() {
        val resource = detailMovie.value
        if (resource?.data != null) {
            val newState = !resource.data.isFav
            imdbRepository.setFavoriteMovie(resource.data, newState)
        }
    }

    fun fakeSetFavoriteMovie(movieEntity: MovieEntity) {
        val resource = movieEntity
        if (resource != null) {
            val newState = !resource.isFav
            imdbRepository.setFavoriteMovie(resource, newState)
        }
    }
}
