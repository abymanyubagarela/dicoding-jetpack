package com.example.myapplication.views

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import androidx.paging.PagedList
import com.example.myapplication.data.source.ImdbRepository
import com.example.myapplication.data.source.lokal.entity.ShowEntity
import com.example.myapplication.models.DataDummy
import com.example.myapplication.vo.Resource
import org.junit.Assert.assertEquals
import org.junit.Assert.assertNotNull
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.Mockito.*
import org.mockito.junit.MockitoJUnitRunner

@RunWith(MockitoJUnitRunner::class)
class ShowsViewModelTest {

    private lateinit var viewModel: ShowsViewModel

    private val dummy = DataDummy.sampleDataShowDummy()
    private val dummyId = dummy[0].id

    @get:Rule
    var instantTaskExecutorRule = InstantTaskExecutorRule()

    @Mock
    private lateinit var imdbRepository: ImdbRepository

    @Mock
    private lateinit var observer: Observer<Resource<PagedList<ShowEntity>>>

    @Mock
    private lateinit var detailObserver: Observer<Resource<ShowEntity>>

    @Mock
    private lateinit var favObserver: Observer<PagedList<ShowEntity>>

    @Mock
    private lateinit var pagedList: PagedList<ShowEntity>

    @Before
    fun setUp() {
        viewModel = ShowsViewModel(imdbRepository)
    }

    @Test
    fun fetchShows() {
        val dummyShows = Resource.success(pagedList)
        `when`(dummyShows.data?.size).thenReturn(1)
        val shows = MutableLiveData<Resource<PagedList<ShowEntity>>>()
        shows.value = dummyShows

        `when`(imdbRepository.fetchShows()).thenReturn(shows)
        val show = viewModel.fetchShows().value?.data
        verify(imdbRepository).fetchShows()
        assertNotNull(show)
        assertEquals(1, show?.size)

        viewModel.fetchShows().observeForever(observer)
        verify(observer).onChanged(dummyShows)
    }

    @Test
    fun fetchFavShows() {
        val dummyShow = pagedList
        `when`(dummyShow.size).thenReturn(1)
        val shows = MutableLiveData<PagedList<ShowEntity>>()
        shows.value = dummyShow

        `when`(imdbRepository.getFavoriteShows()).thenReturn(shows)
        val show = viewModel.fetchFavShows().value
        verify(imdbRepository).getFavoriteShows()
        assertNotNull(show)
        assertEquals(1, show?.size)
    }

    @Test
    fun fetchShow() {
        val dummyDetailShow = Resource.success(DataDummy.sampleDataShowDummy()[0])
        val show = MutableLiveData<Resource<ShowEntity>>()
        show.value = dummyDetailShow

        `when`(imdbRepository.fetchShow(dummyId)).thenReturn(show)

        viewModel.fetchShow(dummyId).observeForever(detailObserver)
        verify(detailObserver).onChanged(dummyDetailShow)
    }

    @Test
    fun setFavoriteShow() {
        val dummyDetail = Resource.success(dummy[0])
        val show = MutableLiveData<Resource<ShowEntity>>()
        show.value = dummyDetail

        viewModel.fakeSetFavoriteShow(dummy[0])
        verify(imdbRepository).setFavoriteShow(show.value!!.data as ShowEntity, true)
        verifyNoMoreInteractions(favObserver)
    }
}