package com.example.myapplication.views

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import androidx.paging.PagedList
import com.example.myapplication.data.source.ImdbRepository
import com.example.myapplication.data.source.lokal.entity.MovieEntity
import com.example.myapplication.models.DataDummy
import com.example.myapplication.models.Movies
import com.example.myapplication.vo.Resource
import org.junit.Assert.assertEquals
import org.junit.Assert.assertNotNull
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.Mockito.*
import org.mockito.junit.MockitoJUnitRunner

@RunWith(MockitoJUnitRunner::class)
class MoviesViewModelTest {

    private lateinit var viewModel: MoviesViewModel

    private val dummy = DataDummy.sampleDataMovieDummy()
    private val dummyId = dummy[0].id

    @get:Rule
    var instantTaskExecutorRule = InstantTaskExecutorRule()

    @Mock
    private lateinit var imdbRepository: ImdbRepository

    @Mock
    private lateinit var observer: Observer<Resource<PagedList<MovieEntity>>>

    @Mock
    private lateinit var detailObserver: Observer<Resource<MovieEntity>>

    @Mock
    private lateinit var favObserver: Observer<PagedList<MovieEntity>>

    @Mock
    private lateinit var pagedList: PagedList<MovieEntity>

    @Before
    fun setUp() {
        viewModel = MoviesViewModel(imdbRepository)
    }

    @Test
    fun fetchMovies() {
        val dummyMovies = Resource.success(pagedList)
        `when`(dummyMovies.data?.size).thenReturn(1)
        val movies = MutableLiveData<Resource<PagedList<MovieEntity>>>()
        movies.value = dummyMovies

        `when`(imdbRepository.fetchMovies()).thenReturn(movies)
        val movie = viewModel.fetchMovies().value?.data
        verify(imdbRepository).fetchMovies()
        assertNotNull(movie)
        assertEquals(1, movie?.size)

        viewModel.fetchMovies().observeForever(observer)
        verify(observer).onChanged(dummyMovies)
    }

    @Test
    fun fetchFavMovies() {
        val dummyMovie = pagedList
        `when`(dummyMovie.size).thenReturn(1)
        val movies = MutableLiveData<PagedList<MovieEntity>>()
        movies.value = dummyMovie

        `when`(imdbRepository.getFavoriteMovies()).thenReturn(movies)
        val movie = viewModel.fetchFavMovies().value
        verify(imdbRepository).getFavoriteMovies()
        assertNotNull(movie)
        assertEquals(1, movie?.size)
    }

    @Test
    fun fetchMovie() {
        val dummyDetailMovie = Resource.success(DataDummy.sampleDataMovieDummy()[0])
        val movie = MutableLiveData<Resource<MovieEntity>>()
        movie.value = dummyDetailMovie

        `when`(imdbRepository.fetchMovie(dummyId)).thenReturn(movie)

        viewModel.fetchMovie(dummyId).observeForever(detailObserver)
        verify(detailObserver).onChanged(dummyDetailMovie)
    }

    @Test
    fun setFavoriteMovie() {
        val dummyDetail = Resource.success(dummy[0])
        val movie = MutableLiveData<Resource<MovieEntity>>()
        movie.value = dummyDetail

        viewModel.fakeSetFavoriteMovie(dummy[0])
        verify(imdbRepository).setFavoriteMovie(movie.value!!.data as MovieEntity, true)
        verifyNoMoreInteractions(favObserver)
    }
}