package com.example.myapplication.data.source

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.paging.LivePagedListBuilder
import androidx.paging.PagedList
import com.example.myapplication.data.source.lokal.LocalDataSource
import com.example.myapplication.data.source.lokal.entity.MovieEntity
import com.example.myapplication.data.source.lokal.entity.ShowEntity
import com.example.myapplication.data.source.remote.RemoteDataSource
import com.example.myapplication.data.source.remote.response.ApiResponse
import com.example.myapplication.models.Movies
import com.example.myapplication.models.Shows
import com.example.myapplication.utils.AppExecutors
import com.example.myapplication.vo.Resource

class FakeImdbRepository(
    private val remoteDataSource: RemoteDataSource,
    private val localDataSource: LocalDataSource,
    private val appExecutors: AppExecutors
) :
    ImdbDataSource {

    override fun fetchMovies(): LiveData<Resource<PagedList<MovieEntity>>> {
        return object : NetworkBoundResource<PagedList<MovieEntity>, List<Movies>>(appExecutors) {
            override fun loadFromDb(): LiveData<PagedList<MovieEntity>> {
                val config = PagedList.Config.Builder()
                    .setEnablePlaceholders(false)
                    .setInitialLoadSizeHint(4)
                    .setPageSize(4)
                    .build()
                return LivePagedListBuilder(localDataSource.getAllMovies(), config).build()
            }

            override fun shouldFetch(data: PagedList<MovieEntity>?): Boolean {
                return data == null || data.isEmpty()
            }

            override fun createCall(): LiveData<ApiResponse<List<Movies>>> {
                return remoteDataSource.fetchMovies()
            }

            override fun saveCallResult(data: List<Movies>) {
                val list = ArrayList<MovieEntity>()
                for (response in data) {
                    val movie = MovieEntity(
                        id = response.id.toString(),
                        title = response.title.toString(),
                        overview = response.overview.toString(),
                        popularity = response.popularity.toString(),
                        poster_path = response.poster_path.toString(),
                        vote_average = response.vote_average.toString(),
                        vote_count = response.vote_count.toString(),
                        backdrop_path = response.backdrop_path.toString(),
                        isFav = false
                    )
                    list.add(movie)
                }
                localDataSource.insertMovies(list)
            }
        }.asLiveData()
    }

    override fun fetchMovie(id: String?): LiveData<Resource<MovieEntity>> {
        return object : NetworkBoundResource<MovieEntity, Movies>(appExecutors) {
            override fun loadFromDb(): LiveData<MovieEntity> {
                return localDataSource.getMovieById(id.toString())
            }

            override fun shouldFetch(data: MovieEntity?): Boolean {
                return data == null
            }

            override fun createCall(): LiveData<ApiResponse<Movies>> {
                return remoteDataSource.fetchMovie(id.toString())
            }

            override fun saveCallResult(data: Movies) {
                val movie = MovieEntity(
                    id = data.id.toString(),
                    title = data.title.toString(),
                    overview = data.overview.toString(),
                    popularity = data.popularity.toString(),
                    poster_path = data.poster_path.toString(),
                    vote_average = data.vote_average.toString(),
                    vote_count = data.vote_count.toString(),
                    backdrop_path = data.backdrop_path.toString(),
                    isFav = false
                )
                localDataSource.updateMovie(movie, false)
            }

        }.asLiveData()
    }

    override fun getFavoriteMovies(): LiveData<PagedList<MovieEntity>> {
        val config = PagedList.Config.Builder()
            .setEnablePlaceholders(false)
            .setInitialLoadSizeHint(4)
            .setPageSize(4)
            .build()
        return LivePagedListBuilder(localDataSource.getAllFavMovies(), config).build()
    }

    override fun setFavoriteMovie(movie: MovieEntity, state: Boolean) {
        appExecutors.diskIO().execute {
            localDataSource.setFavMovie(movie, state)
        }
    }

    override fun fetchShows(): LiveData<Resource<PagedList<ShowEntity>>> {
        return object : NetworkBoundResource<PagedList<ShowEntity>, List<Shows>>(appExecutors) {
            override fun loadFromDb(): LiveData<PagedList<ShowEntity>> {
                val config = PagedList.Config.Builder()
                    .setEnablePlaceholders(false)
                    .setInitialLoadSizeHint(4)
                    .setPageSize(4)
                    .build()
                return LivePagedListBuilder(localDataSource.getAllShows(), config).build()
            }

            override fun shouldFetch(data: PagedList<ShowEntity>?): Boolean {
                return data == null || data.isEmpty()
            }

            override fun createCall(): LiveData<ApiResponse<List<Shows>>> {
                return remoteDataSource.fetchShows()
            }

            override fun saveCallResult(data: List<Shows>) {
                val list = ArrayList<ShowEntity>()
                for (response in data) {
                    val show = ShowEntity(
                        id = response.id.toString(),
                        title = response.title.toString(),
                        name = response.name.toString(),
                        overview = response.overview.toString(),
                        popularity = response.popularity.toString(),
                        poster_path = response.poster_path.toString(),
                        vote_average = response.vote_average.toString(),
                        vote_count = response.vote_count.toString(),
                        backdrop_path = response.backdrop_path.toString(),
                        isFav = false
                    )
                    list.add(show)
                }
                localDataSource.insertShows(list)
            }
        }.asLiveData()
    }

    override fun fetchShow(id: String?): LiveData<Resource<ShowEntity>> {
        return object : NetworkBoundResource<ShowEntity, Shows>(appExecutors) {
            override fun loadFromDb(): LiveData<ShowEntity> {
                return localDataSource.getShowById(id.toString())
            }

            override fun shouldFetch(data: ShowEntity?): Boolean {
                return data == null
            }

            override fun createCall(): LiveData<ApiResponse<Shows>> {
                return remoteDataSource.fetchShow(id.toString())
            }

            override fun saveCallResult(data: Shows) {
                val show = ShowEntity(
                    id = data.id.toString(),
                    title = data.title.toString(),
                    name = data.name.toString(),
                    overview = data.overview.toString(),
                    popularity = data.popularity.toString(),
                    poster_path = data.poster_path.toString(),
                    vote_average = data.vote_average.toString(),
                    vote_count = data.vote_count.toString(),
                    backdrop_path = data.backdrop_path.toString(),
                    isFav = false
                )
                localDataSource.updateShow(show, false)
            }

        }.asLiveData()
    }

    override fun getFavoriteShows(): LiveData<PagedList<ShowEntity>> {
        val config = PagedList.Config.Builder()
            .setEnablePlaceholders(false)
            .setInitialLoadSizeHint(4)
            .setPageSize(4)
            .build()
        return LivePagedListBuilder(localDataSource.getAllFavShows(), config).build()
    }

    override fun setFavoriteShow(show: ShowEntity, state: Boolean) {
        appExecutors.diskIO().execute {
            localDataSource.setFavShowById(show.isFav, show.id)
        }
    }

}