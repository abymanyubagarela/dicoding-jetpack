package com.example.myapplication.data.source

import android.util.Log
import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.MutableLiveData
import androidx.paging.DataSource
import com.example.myapplication.data.source.lokal.LocalDataSource
import com.example.myapplication.data.source.lokal.entity.MovieEntity
import com.example.myapplication.data.source.lokal.entity.ShowEntity
import com.example.myapplication.data.source.remote.RemoteDataSource
import com.example.myapplication.models.DataDummy
import com.example.myapplication.utils.AppExecutors
import com.example.myapplication.utils.LiveDataTestUtil
import com.example.myapplication.utils.PagedListUtil
import com.example.myapplication.vo.Resource
import org.junit.Assert.assertEquals
import org.junit.Assert.assertNotNull
import org.junit.Rule
import org.junit.Test
import org.mockito.Mockito.*

class ImdbRepositoryTest {
    @get:Rule
    var instantTaskExecutorRule = InstantTaskExecutorRule()

    private val remote = mock(RemoteDataSource::class.java)
    private val local = mock(LocalDataSource::class.java)
    private val appExecutors = mock(AppExecutors::class.java)
    private val fakeImdbRepository = FakeImdbRepository(remote, local, appExecutors)

    private val movies = DataDummy.sampleDataMovieDummy()
    private val movieId = movies[0].id
    private val movieDetail = movies[0]

    private val shows = DataDummy.sampleDataShowDummy()
    private val showId = shows[0].id
    private val showDetail = shows[0]

    @Test
    fun fetchMovies() {
        val dataSourceFactory =
            mock(DataSource.Factory::class.java) as DataSource.Factory<Int, MovieEntity>
        `when`(local.getAllMovies()).thenReturn(dataSourceFactory)
        fakeImdbRepository.fetchMovies()

        val entity = Resource.success(PagedListUtil.mockPagedList(DataDummy.sampleDataMovieDummy()))
        verify(local).getAllMovies()
        assertNotNull(entity)
        assertEquals(movies.size, entity.data?.size)
    }

    @Test
    fun fetchShows() {
        val dataSourceFactory =
            mock(DataSource.Factory::class.java) as DataSource.Factory<Int, ShowEntity>
        `when`(local.getAllShows()).thenReturn(dataSourceFactory)
        fakeImdbRepository.fetchShows()

        val entity = Resource.success(PagedListUtil.mockPagedList(DataDummy.sampleDataShowDummy()))
        verify(local).getAllShows()
        assertNotNull(entity)
        assertEquals(shows.size, entity.data?.size)
    }

    @Test
    fun getFavoriteMovies() {
        val dataSourceFactory =
            mock(DataSource.Factory::class.java) as DataSource.Factory<Int, MovieEntity>
        `when`(local.getAllFavMovies()).thenReturn(dataSourceFactory)
        fakeImdbRepository.getFavoriteMovies()

        val entity = Resource.success(PagedListUtil.mockPagedList(DataDummy.sampleDataMovieDummy()))
        verify(local).getAllFavMovies()
        assertNotNull(entity)
        assertEquals(movies.size, entity.data?.size)
    }

    @Test
    fun setFavoriteMovie() {
        /* test case di hapus berdasar arahan mentor , tes favourite via unit test
        fakeImdbRepository.setFavoriteMovie(movieDetail, true)
        verify(local).setFavMovie(movieDetail, true)
        verifyNoMoreInteractions(local)
         */
    }

    @Test
    fun fetchMovie() {
        val detail = MutableLiveData<MovieEntity>()
        detail.value = movieDetail
        `when`(local.getMovieById(movieId)).thenReturn(detail)

        val entity = LiveDataTestUtil.getValue(fakeImdbRepository.fetchMovie(movieId))
        verify(local).getMovieById(movieId)
        assertNotNull(entity)
        assertEquals(movieDetail.id, entity.data?.id)
    }

    @Test
    fun fetchShow() {
        val detail = MutableLiveData<ShowEntity>()
        detail.value = showDetail
        `when`(local.getShowById(showId)).thenReturn(detail)

        val entity = LiveDataTestUtil.getValue(fakeImdbRepository.fetchShow(showId))
        verify(local).getShowById(showId)
        assertNotNull(entity)
        assertEquals(showDetail.id, entity.data?.id)
    }

    @Test
    fun getFavoriteShows() {
        val dataSourceFactory =
            mock(DataSource.Factory::class.java) as DataSource.Factory<Int, ShowEntity>
        `when`(local.getAllFavShows()).thenReturn(dataSourceFactory)
        fakeImdbRepository.getFavoriteShows()

        val entity = Resource.success(PagedListUtil.mockPagedList(shows))
        verify(local).getAllFavShows()
        assertNotNull(entity)
        assertEquals(shows.size, entity.data?.size)

        System.out.println("Test ${shows.size}")
    }

    @Test
    fun setFavoriteShow() {
/* test case di hapus berdasar arahan mentor , tes favourite via unit test
fakeImdbRepository.setFavoriteShow(DataDummy.getDetailShowDummy(), true)
verify(local).setFavShow(DataDummy.getDetailShowDummy(), true)
verifyNoMoreInteractions(local)

 */
    }
}